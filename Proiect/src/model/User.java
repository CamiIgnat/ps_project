package model;

public class User extends Entity{
	private int id;
	private String name;
	private String cnp;
	private String address;
	private String username;
	private String password;
	private String type;
	
	public User() {
		
	}
	
	public User(int id,String name,String cnp,String address,String username,String password,String type) {
		this.id = id;
		this.name = name;
		this.cnp = cnp;
		this.address = address;
		this.username = username;
		this.password = password;
		this.type = type;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCnp() {
		return cnp;
	}

	public void setCnp(String cnp) {
		this.cnp = cnp;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	public String[] asRow() {
		String[] row = { String.valueOf(this.getId()), this.getName(), this.getCnp(), this.getAddress(), this.getUsername(), this.getPassword(), this.getType() }; 
		
		return row;
	}
}
