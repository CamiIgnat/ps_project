package model;

public class Movie extends Entity {
	private int id;
	private String name;
	private String regizor;
	private String description;
	private double rating;
	private String genre;
	private int quantity;
	private double price;
	
	public Movie() {
		
	}
	
	public Movie(int id,String name,String regizor,String description,double rating,String genre,int quantity,double price) {
		this.id = id;
		this.name = name;
		this.regizor = regizor;
		this.description = description;
		this.rating = rating;
		this.genre = genre;
		this.quantity = quantity;
		this.price = price;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRegizor() {
		return regizor;
	}

	public void setRegizor(String regizor) {
		this.regizor = regizor;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public double getRating() {
		return rating;
	}

	public void setRating(double rating) {
		this.rating = rating;
	}

	public String getGenre() {
		return genre;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}
	
	public String[] asRow() {
		String[] row = { String.valueOf(this.getId()), this.getName(), this.getRegizor(), this.getDescription(), String.valueOf(this.getRating()), this.getGenre(), String.valueOf(this.getQuantity()), String.valueOf(this.getPrice()) }; 
		
		return row;
	}
}
