package model;

import java.sql.Date;

public class Order extends Entity {
	private int id;
	private int userId;
	private int movieId;
	private int quantity;
	private double price;
	private Date date;
	private String status;
	
	public Order() {
		
	}
	
	public Order(int id,int userId,int movieId,int quantity,double price,Date date,String status) {
		this.id = id;
		this.userId = userId;
		this.movieId = movieId;
		this.quantity = quantity;
		this.price = price;
		this.date = date;
		this.status = status;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getMovieId() {
		return movieId;
	}

	public void setMovieId(int movieId) {
		this.movieId = movieId;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
