package businessLayer;

import model.Movie;

public class Rating {
	private Repository repository;
	public void ratingForMovie(Movie movie, double rating) {
		double oldRating = movie.getRating();
		
		if (oldRating == 0.0)
			movie.setRating(rating);
		else movie.setRating((oldRating+rating)/2);
		
		repository = new Repository(movie);
		repository.update();
	}
}
