package businessLayer;

import java.sql.ResultSet;

import dataAccessLayer.*;
import model.*;

public class Repository<TGateway extends Gateway<TEntity>, TEntity extends Entity> {

	private TGateway gateway;
	private TEntity entity;
	
	@SuppressWarnings("unchecked")
	public Repository(TEntity entity) {
		this.entity = entity;
		if(entity instanceof User) 
			this.gateway = (TGateway) new UserGateway();
		
		if(entity instanceof Movie) 
			this.gateway = (TGateway) new MovieGateway();
		
		if(entity instanceof Order) 
			this.gateway = (TGateway) new OrderGateway();
		
	}

	public int insert() {
		return gateway.insert(entity);
	}
	
	public void delete() {
		gateway.delete(entity);
	}
	
	public void update() {
		gateway.update(entity);
	}
	
	public ResultSet findById(int id) {
		return gateway.findById(id);
	}
	
	public ResultSet findAll() {
		return gateway.findAll();
	}
}