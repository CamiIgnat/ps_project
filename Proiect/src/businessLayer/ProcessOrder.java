package businessLayer;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import dataAccessLayer.MovieGateway;
import dataAccessLayer.OrderGateway;
import model.Movie;
import model.Order;

public class ProcessOrder {
	private Repository repository;
	private OrderGateway orderGateway;
	private MovieGateway moviewGateway;
	public int processOrder(int userId, Movie movie, int quantity) {
		
		if(movie.getQuantity() > movie.getQuantity())
			return -1;
		int oldQuantity = movie.getQuantity();
		
		Order order = new Order();
		order.setUserId(userId);
		order.setMovieId(movie.getId());
		order.setQuantity(quantity);
		order.setPrice(quantity*movie.getPrice());
		order.setDate(new java.sql.Date(new Date().getTime()));
		order.setStatus("Waiting");
		repository = new Repository(order);
		repository.insert();
		
		
		movie.setQuantity(oldQuantity-quantity);
		repository = new Repository(movie);
		repository.update();
		
		return 1;
	}
}
