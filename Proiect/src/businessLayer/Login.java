package businessLayer;

import java.sql.ResultSet;
import java.sql.SQLException;

import dataAccessLayer.UserGateway;
import model.User;

public class Login {
	public User login(String username, String password) {
		
		User user = new User();
		UserGateway gateway = new UserGateway();
		ResultSet rez = gateway.findAll();
		try{
			while(rez.next()) {
				if(rez.getString("username").equals(username) && rez.getString("password").equals(password)) {
					user.setId(rez.getInt("id"));
					user.setName(rez.getString("name"));
					user.setCnp(rez.getString("cnp"));
					user.setAddress(rez.getString("address"));
					user.setUsername(rez.getString("username"));
					user.setPassword(rez.getString("password"));
					user.setType(rez.getString("type"));
				}
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return user;
	}
}
