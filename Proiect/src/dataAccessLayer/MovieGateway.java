package dataAccessLayer;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import model.Movie;
import model.User;

public class MovieGateway extends Gateway<Movie> {
	
	public MovieGateway() {
		super();
	}
	@Override
	public int insert(Movie movie) {
		try{
			String query = "INSERT INTO `movie` (`id`, `name`, `regizor`, `description`, `rating`, `genre`, `quantity`, `price`) VALUES (NULL, ?, ?, ?, ?, ?, ?, ?)";
			PreparedStatement statement = this.getDbConnectionn().getConnection().prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
			statement.setString(1, movie.getName());
			statement.setString(2, movie.getRegizor());
			statement.setString(3, movie.getDescription());
			statement.setDouble(4, movie.getRating());
			statement.setString(5, movie.getGenre());
			statement.setInt(6, movie.getQuantity());
			statement.setDouble(7, movie.getPrice());
			statement.executeUpdate();
			ResultSet rez = statement.getGeneratedKeys();
			while(rez.next()) 
				return rez.getInt(1);
		} catch (Exception e) {
			System.out.println("Error"+e);
		}
		return 0;
	}

	@Override
	public void delete(Movie movie) {
		try {
			String query = "DELETE FROM `movie` WHERE id=?";
			PreparedStatement statement = this.getDbConnectionn().getConnection().prepareStatement(query);
			statement.setInt(1, movie.getId());
			statement.execute();
		} catch (SQLException e) {
			System.out.println("Error"+e);
		}
	}

	@Override
	public void update(Movie movie) {
		try {
			String query = "UPDATE `movie` SET `name` = ?, `regizor` = ?, `description` = ?, `rating` = ?, `genre` = ?, `quantity` = ?, `price` = ? WHERE `movie`.`id` = ?";
			PreparedStatement statement = this.getDbConnectionn().getConnection().prepareStatement(query);
			statement.setString(1, movie.getName());
			statement.setString(2, movie.getRegizor());
			statement.setString(3, movie.getDescription());
			statement.setDouble(4, movie.getRating());
			statement.setString(5, movie.getGenre());
			statement.setInt(6, movie.getQuantity());
			statement.setDouble(7, movie.getPrice());
			statement.setInt(8, movie.getId());
			statement.executeUpdate();
		} catch (SQLException e) {
			System.out.println("Error"+e);
		}
	}

	@Override
	public ResultSet findById(int id) {
		try {
			String query = "select * from movie where id=?";
			PreparedStatement statement = this.getDbConnectionn().getConnection().prepareStatement(query);
			statement.setInt(1, id);
			return statement.executeQuery();
		} catch (SQLException e) {
			System.out.println("Error"+e);
		}
		return null;
	}

	@Override
	public ResultSet findAll() {
		try {
			String query = "select * from movie";
			PreparedStatement statement = this.getDbConnectionn().getConnection().prepareStatement(query);
			return statement.executeQuery();
		} catch (SQLException e) {
			System.out.println("Error"+e);
		}
		return null;
	}
}
