package dataAccessLayer;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import model.Order;

public class OrderGateway extends Gateway<Order>{
	@Override
	public int insert(Order order) {
		try{
			String query = "INSERT INTO `order` (`id`, `userId`, `movieId`, `quantity`, `price`, `date`, `status`) VALUES (NULL, ?, ?, ?, ?, ?, ?)";
			PreparedStatement statement = this.getDbConnectionn().getConnection().prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
			statement.setInt(1, order.getUserId());
			statement.setInt(2, order.getMovieId());
			statement.setDouble(3, order.getQuantity());
			statement.setDouble(4,  order.getPrice());
			statement.setDate(5, order.getDate());
			statement.setString(6, order.getStatus());
			statement.executeUpdate();
			ResultSet rez = statement.getGeneratedKeys();
			while(rez.next()) 
				return rez.getInt(1);
		} catch (Exception e) {
			System.out.println("Error"+e);
		}
		return 0;
	}

	@Override
	public void delete(Order order) {
		try {
			String query = "DELETE FROM `order` WHERE id=?";
			PreparedStatement statement = this.getDbConnectionn().getConnection().prepareStatement(query);
			statement.setInt(1, order.getId());
			statement.execute();
		} catch (SQLException e) {
			System.out.println("Error"+e);
		}		
	}

	@Override
	public void update(Order order) {
		try {
			String query = "UPDATE `order` SET `userId` = ?, `movieId` = ?, `quantity` = ?, `price` = ?, `date` = ?, `status` = ? WHERE `order`.`id` = ?";
			PreparedStatement statement = this.getDbConnectionn().getConnection().prepareStatement(query);
			statement.setInt(1, order.getUserId());
			statement.setInt(2, order.getMovieId());
			statement.setDouble(3, order.getQuantity());
			statement.setDouble(4, order.getPrice()); 
			statement.setDate(5, order.getDate());
			statement.setString(6, order.getStatus());
			statement.setInt(7, order.getId());
			statement.execute();
		} catch (SQLException e) {
			System.out.println("Error"+e);
		}		
	}

	@Override
	public ResultSet findById(int id) {
		try {
			String query = "select * from `order` where `order`.`id`=?";
			PreparedStatement statement = this.getDbConnectionn().getConnection().prepareStatement(query);
			statement.setInt(1, id);
			return statement.executeQuery();
		} catch (SQLException e) {
			System.out.println("Error"+e);
		}
		return null;
	}

	@Override
	public ResultSet findAll() {
		try {
			String query = "select * from `order`";
			PreparedStatement statement = this.getDbConnectionn().getConnection().prepareStatement(query);
			return statement.executeQuery();
		} catch (SQLException e) {
			System.out.println("Error"+e);
		}
		return null;
	}

	

}
