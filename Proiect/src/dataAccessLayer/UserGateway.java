package dataAccessLayer;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import model.User;

public class UserGateway extends Gateway<User>{
	
	public UserGateway() {
		super();
	}
	@Override
	public int insert(User user) {
		try{
			String query = "INSERT INTO `user` (`id`, `name`, `cnp`, `address`, `username`, `password`, `type`) VALUES (NULL, ?, ?, ?, ?, ?, ?)";
			PreparedStatement statement = this.getDbConnectionn().getConnection().prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
			statement.setString(1, user.getName());
			statement.setString(2, user.getCnp());
			statement.setString(3, user.getAddress());
			statement.setString(4, user.getUsername());
			statement.setString(5, user.getPassword());
			statement.setString(6, user.getType());
			statement.executeUpdate();
			ResultSet rez = statement.getGeneratedKeys();
			while(rez.next()) 
				return rez.getInt(1);
		} catch (Exception e) {
			System.out.println("Error"+e);
		}
		return 0;
	}

	@Override
	public void delete(User user) {
		try {
			String query = "DELETE FROM `user` WHERE id=?";
			PreparedStatement statement = this.getDbConnectionn().getConnection().prepareStatement(query);
			statement.setInt(1, user.getId());
			statement.execute();
		} catch (SQLException e) {
			System.out.println("Error"+e);
		}
	}

	@Override
	public void update(User user) {
		try {
			String query = "UPDATE `user` SET `name` = ?, `cnp` = ?, `address` = ?, `username` = ?, `password` = ?, `type` = ? WHERE `user`.`id` = ?";
			PreparedStatement statement = this.getDbConnectionn().getConnection().prepareStatement(query);
			statement.setString(1, user.getName());
			statement.setString(2, user.getCnp());
			statement.setString(3, user.getAddress());
			statement.setString(4, user.getUsername());
			statement.setString(5, user.getPassword());
			statement.setString(6, user.getType());
			statement.setInt(7, user.getId());
			statement.execute();
		} catch (SQLException e) {
			System.out.println("Error"+e);
		}
	}

	@Override
	public ResultSet findById(int id) {
		try {
			String query = "select * from user where id=?";
			PreparedStatement statement = this.getDbConnectionn().getConnection().prepareStatement(query);
			statement.setInt(1, id);
			return statement.executeQuery();
		} catch (SQLException e) {
			System.out.println("Error"+e);
		}
		return null;
	}

	@Override
	public ResultSet findAll() {
		try {
			String query = "select * from user";
			PreparedStatement statement = this.getDbConnectionn().getConnection().prepareStatement(query);
			return statement.executeQuery();
		} catch (SQLException e) {
			System.out.println("Error"+e);
		}
		return null;
	}

}
