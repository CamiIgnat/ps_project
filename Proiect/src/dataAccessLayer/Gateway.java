package dataAccessLayer;

import java.sql.ResultSet;

import model.Entity;

public abstract class Gateway<TEntity extends Entity> {
	private DBConnection DbConnection;
	
	public Gateway(){
		DbConnection=new DBConnection();
	}
	
	public DBConnection getDbConnectionn() {
		return DbConnection;
	}

	public void setDbConnection(DBConnection connection) {
		this.DbConnection = connection;
	}

	public abstract int insert(TEntity e);
	
	public abstract void delete(TEntity e);
	public abstract void update(TEntity e);
	
	public abstract ResultSet findById(int id);
	
	public abstract ResultSet findAll();
}
