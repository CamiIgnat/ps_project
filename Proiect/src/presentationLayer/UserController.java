package presentationLayer;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;

import businessLayer.ProcessOrder;
import businessLayer.Rating;
import businessLayer.Repository;
import model.Movie;
import model.Order;
import model.User;

public class UserController {
	private UserView userView;
	private Repository repository;
	
	public UserController() {
		
	}
	
	public UserController(UserView userView) {
		this.userView = userView;
	}
	
	public void loadAllMovies() {
		userView.clearMoviesTable();
		repository = new Repository(new Movie());
		ResultSet rez = repository.findAll();
		
		try {
			while(rez.next()) {
				int id = rez.getInt("id");
				String name = rez.getString("name");
				String regizor = rez.getString("regizor");
				String description = rez.getString("description");
				Double rating = rez.getDouble("rating");
				String genre = rez.getString("genre");
				int quantity = rez.getInt("quantity");
				Double price = rez.getDouble("price");
				String[] row = { String.valueOf(id), name, regizor, description, String.valueOf(rating), genre, String.valueOf(quantity), String.valueOf(price) };
				userView.insertMovie(row);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void searchAction() {
		String searchedMovie = "Action";
		repository = new Repository(new Movie());
		ResultSet rez = repository.findAll();
		userView.clearMoviesTable();
		
		try {
			while(rez.next()) {
				int id = rez.getInt("id");
				String name = rez.getString("name");
				String regizor = rez.getString("regizor");
				String description = rez.getString("description");
				Double rating = rez.getDouble("rating");
				String genre = rez.getString("genre");
				int quantity = rez.getInt("quantity");
				Double price = rez.getDouble("price");
				String[] row = { String.valueOf(id), name, regizor, description, String.valueOf(rating), genre, String.valueOf(quantity), String.valueOf(price) };
				if (genre.toLowerCase().contains(searchedMovie.toLowerCase())) {
					userView.insertMovie(row);
				}
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public void searchDrama() {
		String searchedMovie = "Drama";
		repository = new Repository(new Movie());
		ResultSet rez = repository.findAll();
		userView.clearMoviesTable();
		
		try {
			while(rez.next()) {
				int id = rez.getInt("id");
				String name = rez.getString("name");
				String regizor = rez.getString("regizor");
				String description = rez.getString("description");
				Double rating = rez.getDouble("rating");
				String genre = rez.getString("genre");
				int quantity = rez.getInt("quantity");
				Double price = rez.getDouble("price");
				String[] row = { String.valueOf(id), name, regizor, description, String.valueOf(rating), genre, String.valueOf(quantity), String.valueOf(price) };
				if (genre.toLowerCase().contains(searchedMovie.toLowerCase())) {
					userView.insertMovie(row);
				}
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public void searchComedy() {
		String searchedMovie = "Comedy";
		repository = new Repository(new Movie());
		ResultSet rez = repository.findAll();
		userView.clearMoviesTable();
		
		try {
			while(rez.next()) {
				int id = rez.getInt("id");
				String name = rez.getString("name");
				String regizor = rez.getString("regizor");
				String description = rez.getString("description");
				Double rating = rez.getDouble("rating");
				String genre = rez.getString("genre");
				int quantity = rez.getInt("quantity");
				Double price = rez.getDouble("price");
				String[] row = { String.valueOf(id), name, regizor, description, String.valueOf(rating), genre, String.valueOf(quantity), String.valueOf(price) };
				if (genre.toLowerCase().contains(searchedMovie.toLowerCase())) {
					userView.insertMovie(row);
				}
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public void searchHorror() {
		String searchedMovie = "Horror";
		repository = new Repository(new Movie());
		ResultSet rez = repository.findAll();
		userView.clearMoviesTable();
		
		try {
			while(rez.next()) {
				int id = rez.getInt("id");
				String name = rez.getString("name");
				String regizor = rez.getString("regizor");
				String description = rez.getString("description");
				Double rating = rez.getDouble("rating");
				String genre = rez.getString("genre");
				int quantity = rez.getInt("quantity");
				Double price = rez.getDouble("price");
				String[] row = { String.valueOf(id), name, regizor, description, String.valueOf(rating), genre, String.valueOf(quantity), String.valueOf(price) };
				if (genre.toLowerCase().contains(searchedMovie.toLowerCase())) {
					userView.insertMovie(row);
				}
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public void searchRomance() {
		String searchedMovie = "Romance";
		repository = new Repository(new Movie());
		ResultSet rez = repository.findAll();
		userView.clearMoviesTable();
		
		try {
			while(rez.next()) {
				int id = rez.getInt("id");
				String name = rez.getString("name");
				String regizor = rez.getString("regizor");
				String description = rez.getString("description");
				Double rating = rez.getDouble("rating");
				String genre = rez.getString("genre");
				int quantity = rez.getInt("quantity");
				Double price = rez.getDouble("price");
				String[] row = { String.valueOf(id), name, regizor, description, String.valueOf(rating), genre, String.valueOf(quantity), String.valueOf(price) };
				if (genre.toLowerCase().contains(searchedMovie.toLowerCase())) {
					userView.insertMovie(row);
				}
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public void searchMovieByTitle() {
		String searchedMovieTitle = userView.getSearchedMovie();
		repository = new Repository(new Movie());
		ResultSet rez = repository.findAll();
		userView.clearMoviesTable();
		
		try {
			while(rez.next()) {
				int id = rez.getInt("id");
				String name = rez.getString("name");
				String regizor = rez.getString("regizor");
				String description = rez.getString("description");
				Double rating = rez.getDouble("rating");
				String genre = rez.getString("genre");
				int quantity = rez.getInt("quantity");
				Double price = rez.getDouble("price");
				String[] row = { String.valueOf(id), name, regizor, description, String.valueOf(rating), genre, String.valueOf(quantity), String.valueOf(price) };
				if (name.toLowerCase().contains(searchedMovieTitle.toLowerCase())) {
					userView.insertMovie(row);
				}
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public void searchMovieByRegizor() {
		String searchedMovie = userView.getSearchedMovie();
		repository = new Repository(new Movie());
		ResultSet rez = repository.findAll();
		userView.clearMoviesTable();
		
		try {
			while(rez.next()) {
				int id = rez.getInt("id");
				String name = rez.getString("name");
				String regizor = rez.getString("regizor");
				String description = rez.getString("description");
				Double rating = rez.getDouble("rating");
				String genre = rez.getString("genre");
				int quantity = rez.getInt("quantity");
				Double price = rez.getDouble("price");
				String[] row = { String.valueOf(id), name, regizor, description, String.valueOf(rating), genre, String.valueOf(quantity), String.valueOf(price) };
				if (regizor.toLowerCase().contains(searchedMovie.toLowerCase())) {
					userView.insertMovie(row);
				}
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public void sellMovie() {
		Movie movie = userView.getSellMovie();
		int quantity = userView.getMovieQuantity();
		User user = userView.getCurrentUser();
		
		ProcessOrder po = new ProcessOrder();
		po.processOrder(user.getId(), movie, quantity);
		loadAllMovies();
	}
	
	public void giveRating() {
		double rating = userView.getRating();
		Movie movie = userView.getSellMovie();
		
		Rating rating1 = new Rating();
		rating1.ratingForMovie(movie, rating);
		loadAllMovies();
	}
	
	public void loadAllOrders() {
		userView.clearOrdersTable();
		repository = new Repository(new Order());
		ResultSet rez = repository.findAll();
		User currentUser = userView.getCurrentUser();
		
		try {
			while(rez.next()) {
				int id = rez.getInt("id");
				int userId = rez.getInt("userId");
				int movieId = rez.getInt("movieId");
				int quantity = rez.getInt("quantity");
				Double price = rez.getDouble("price");
				Date date = rez.getDate("date");
				String status = rez.getString("status");
				String[] row = { String.valueOf(id), String.valueOf(userId), String.valueOf(movieId), String.valueOf(quantity), String.valueOf(price), String.valueOf(date), status };
				if (currentUser.getId() == userId)
					userView.insertOrder(row);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void logout() {
		userView.dispose();
	}
}
