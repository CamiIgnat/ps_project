package presentationLayer;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;


public class LoginView extends JFrame {
	private JPanel Panel;

	private JLabel loginLabel;
	private JLabel usernameLabel;
	private JTextField usernameTextField;
	private JLabel passwordLabel;
	private JPasswordField passwordTextField;
	private JButton loginButton;
	private JButton createAccount;
	
	
	public LoginView() {
		Panel = new JPanel();
		Panel.setLayout(null);
		Panel.setBackground(Color.GRAY);
		
		LoginController controller = new LoginController(this);
		
		loginLabel = new JLabel("Login");
		loginLabel.setFont(new Font("Grill Sans MT", Font.BOLD, 22));
		loginLabel.setBounds(300, 30, 100, 50);
		Panel.add(loginLabel);
		
		usernameLabel = new JLabel("Username:");
		usernameLabel.setFont(new Font("Grill Sans MT", Font.BOLD, 12));
		usernameLabel.setBounds(220, 130, 100, 22);
		Panel.add(usernameLabel);
		
		usernameTextField = new JTextField();
		usernameTextField.setBounds(300, 130, 140, 22);
		Panel.add(usernameTextField);
		
		passwordLabel = new JLabel("Password:");
		passwordLabel.setFont(new Font("Grill Sans MT", Font.BOLD, 12));
		passwordLabel.setBounds(220, 180, 100, 22);
		Panel.add(passwordLabel);
		
		passwordTextField = new JPasswordField();
		passwordTextField.setBounds(300, 180, 140, 22);
		Panel.add(passwordTextField);
		
		loginButton = new JButton("Login");
		loginButton.setForeground(Color.white);
		loginButton.setBackground(Color.darkGray);
		loginButton.setBounds(260, 300, 150, 30);
		Panel.add(loginButton);
		loginButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				controller.loginButtonClicked();
			}
		});
		
		
		
		this.setContentPane(Panel);
		this.setBackground(Color.DARK_GRAY);
		this.setVisible(true);
		this.setResizable(false);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setBounds(100,30,700,500);
		this.setTitle("Assignment1");
		
	}

	public JPanel getPanel() {
		return Panel;
	}

	public void setPanel(JPanel panel) {
		Panel = panel;
	}

	public JLabel getUsernameLabel() {
		return usernameLabel;
	}

	public void setUsernameLabel(JLabel usernameLabel) {
		this.usernameLabel = usernameLabel;
	}

	public JTextField getUsernameTextField() {
		return usernameTextField;
	}

	public void setUsernameTextField(JTextField usernameTextField) {
		this.usernameTextField = usernameTextField;
	}

	public JLabel getPasswordLabel() {
		return passwordLabel;
	}

	public void setPasswordLabel(JLabel passwordLabel) {
		this.passwordLabel = passwordLabel;
	}

	public JTextField getPasswordTextField() {
		return passwordTextField;
	}

	public void setPasswordTextField(JPasswordField passwordTextField) {
		this.passwordTextField = passwordTextField;
	}

	public JButton getLoginButton() {
		return loginButton;
	}

	public void setLoginButton(JButton loginButton) {
		this.loginButton = loginButton;
	}

	public JButton getCreateAccount() {
		return createAccount;
	}

	public void setCreateAccount(JButton createAccount) {
		this.createAccount = createAccount;
	}
	
	public String getUsername() {
		return usernameTextField.getText();
	}
	
	public String getPassword() {
		return passwordTextField.getText();
	}
	
	public void displayMessage(String message, String title, int type) {
		JOptionPane.showMessageDialog(this.Panel, message, title, type);
	}
	
}
