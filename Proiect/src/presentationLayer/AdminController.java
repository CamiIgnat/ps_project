package presentationLayer;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;

import businessLayer.Repository;
import model.Movie;
import model.Order;
import model.User;

public class AdminController {
	private Repository repository;
	private AdminView adminView;
	
	public AdminController(AdminView adminView) {
		this.adminView = adminView;
	}
	
	public void loadAllMovies() {
		adminView.clearMoviesTable();
		repository = new Repository(new Movie());
		ResultSet rez = repository.findAll();
		
		try {
			while(rez.next()) {
				int id = rez.getInt("id");
				String name = rez.getString("name");
				String regizor = rez.getString("regizor");
				String description = rez.getString("description");
				Double rating = rez.getDouble("rating");
				String genre = rez.getString("genre");
				int quantity = rez.getInt("quantity");
				Double price = rez.getDouble("price");
				String[] row = { String.valueOf(id), name, regizor, description, String.valueOf(rating), genre, String.valueOf(quantity), String.valueOf(price) };
				adminView.insertMovie(row);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void loadAllUsers() {
		adminView.clearUsersTable();
		repository = new Repository(new User());
		ResultSet rez = repository.findAll();
		
		try {
			while(rez.next()) {
				int id = rez.getInt("id");
				String name = rez.getString("name");
				String cnp = rez.getString("cnp");
				String address = rez.getString("address");
				String username = rez.getString("username");
				String password = rez.getString("password");
				String type = rez.getString("type");
				String[] row = { String.valueOf(id), name, cnp, address, username, password, type };
				adminView.insertUser(row);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void loadAllOrders() {
		adminView.clearOrdersTable();
		repository = new Repository(new Order());
		ResultSet rez = repository.findAll();
		
		try {
			while(rez.next()) {
				int id = rez.getInt("id");
				int userId = rez.getInt("userId");
				int movieId = rez.getInt("movieId");
				int quantity = rez.getInt("quantity");
				Double price = rez.getDouble("price");
				Date date = rez.getDate("date");
				String status = rez.getString("status");
				String[] row = { String.valueOf(id), String.valueOf(userId), String.valueOf(movieId), String.valueOf(quantity), String.valueOf(price), String.valueOf(date), status };
				adminView.insertOrder(row);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void addUser() {
		adminView.clearUsersTable();
		User user = adminView.getUserToCreate();
		repository = new Repository(user);
		repository.insert();
		adminView.insertUser(user.asRow());
		adminView.clearUsersTable();
		loadAllUsers();
	}
	
	public void updateUser() {
		adminView.clearUsersTable();
		User user = adminView.getUserToUpdate();
		repository = new Repository(user);
		repository.update();
		adminView.clearUsersTable();
		loadAllUsers();
	}
	
	public void deleteUser() {
		adminView.clearUsersTable();
		User user = adminView.getUserToDelete();
		repository = new Repository(user);
		repository.delete();
		adminView.clearUsersTable();
		loadAllUsers();
	}
	
	public void addMovie() {
		adminView.clearMoviesTable();
		Movie movie = adminView.getMovieToCreate();
		repository = new Repository(movie);
		repository.insert();
		adminView.insertMovie(movie.asRow());
		adminView.clearMoviesTable();
		loadAllMovies();
	}
	
	public void updateMovie() {
		adminView.clearMoviesTable();
		Movie movie = adminView.getMovieToUpdate();
		repository = new Repository(movie);
		repository.update();
		adminView.clearMoviesTable();
		loadAllMovies();
	}
	
	public void deleteMovie() {
		///adminView.clearMoviesTable();
		Movie movie = adminView.getMovieToDelete();
		repository = new Repository(movie);
		repository.delete();
		adminView.clearMoviesTable();
		loadAllMovies();
	}
	
	public void searchMovieByTitle() {
		String searchedMovieTitle = adminView.getSearchedMovie();
		repository = new Repository(new Movie());
		ResultSet rez = repository.findAll();
		adminView.clearMoviesTable();
		
		try {
			while(rez.next()) {
				int id = rez.getInt("id");
				String name = rez.getString("name");
				String regizor = rez.getString("regizor");
				String description = rez.getString("description");
				Double rating = rez.getDouble("rating");
				String genre = rez.getString("genre");
				int quantity = rez.getInt("quantity");
				Double price = rez.getDouble("price");
				String[] row = { String.valueOf(id), name, regizor, description, String.valueOf(rating), genre, String.valueOf(quantity), String.valueOf(price) };
				if (name.toLowerCase().contains(searchedMovieTitle.toLowerCase())) {
					adminView.insertMovie(row);
				}
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public void searchMovieByRegizor() {
		String searchedMovie = adminView.getSearchedMovie();
		repository = new Repository(new Movie());
		ResultSet rez = repository.findAll();
		adminView.clearMoviesTable();
		
		try {
			while(rez.next()) {
				int id = rez.getInt("id");
				String name = rez.getString("name");
				String regizor = rez.getString("regizor");
				String description = rez.getString("description");
				Double rating = rez.getDouble("rating");
				String genre = rez.getString("genre");
				int quantity = rez.getInt("quantity");
				Double price = rez.getDouble("price");
				String[] row = { String.valueOf(id), name, regizor, description, String.valueOf(rating), genre, String.valueOf(quantity), String.valueOf(price) };
				if (regizor.toLowerCase().contains(searchedMovie.toLowerCase())) {
					adminView.insertMovie(row);
				}
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public void searchAction() {
		String searchedMovie = "Action";
		repository = new Repository(new Movie());
		ResultSet rez = repository.findAll();
		adminView.clearMoviesTable();
		
		try {
			while(rez.next()) {
				int id = rez.getInt("id");
				String name = rez.getString("name");
				String regizor = rez.getString("regizor");
				String description = rez.getString("description");
				Double rating = rez.getDouble("rating");
				String genre = rez.getString("genre");
				int quantity = rez.getInt("quantity");
				Double price = rez.getDouble("price");
				String[] row = { String.valueOf(id), name, regizor, description, String.valueOf(rating), genre, String.valueOf(quantity), String.valueOf(price) };
				if (genre.toLowerCase().contains(searchedMovie.toLowerCase())) {
					adminView.insertMovie(row);
				}
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public void searchDrama() {
		String searchedMovie = "Drama";
		repository = new Repository(new Movie());
		ResultSet rez = repository.findAll();
		adminView.clearMoviesTable();
		
		try {
			while(rez.next()) {
				int id = rez.getInt("id");
				String name = rez.getString("name");
				String regizor = rez.getString("regizor");
				String description = rez.getString("description");
				Double rating = rez.getDouble("rating");
				String genre = rez.getString("genre");
				int quantity = rez.getInt("quantity");
				Double price = rez.getDouble("price");
				String[] row = { String.valueOf(id), name, regizor, description, String.valueOf(rating), genre, String.valueOf(quantity), String.valueOf(price) };
				if (genre.toLowerCase().contains(searchedMovie.toLowerCase())) {
					adminView.insertMovie(row);
				}
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public void searchComedy() {
		String searchedMovie = "Comedy";
		repository = new Repository(new Movie());
		ResultSet rez = repository.findAll();
		adminView.clearMoviesTable();
		
		try {
			while(rez.next()) {
				int id = rez.getInt("id");
				String name = rez.getString("name");
				String regizor = rez.getString("regizor");
				String description = rez.getString("description");
				Double rating = rez.getDouble("rating");
				String genre = rez.getString("genre");
				int quantity = rez.getInt("quantity");
				Double price = rez.getDouble("price");
				String[] row = { String.valueOf(id), name, regizor, description, String.valueOf(rating), genre, String.valueOf(quantity), String.valueOf(price) };
				if (genre.toLowerCase().contains(searchedMovie.toLowerCase())) {
					adminView.insertMovie(row);
				}
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public void searchHorror() {
		String searchedMovie = "Horror";
		repository = new Repository(new Movie());
		ResultSet rez = repository.findAll();
		adminView.clearMoviesTable();
		
		try {
			while(rez.next()) {
				int id = rez.getInt("id");
				String name = rez.getString("name");
				String regizor = rez.getString("regizor");
				String description = rez.getString("description");
				Double rating = rez.getDouble("rating");
				String genre = rez.getString("genre");
				int quantity = rez.getInt("quantity");
				Double price = rez.getDouble("price");
				String[] row = { String.valueOf(id), name, regizor, description, String.valueOf(rating), genre, String.valueOf(quantity), String.valueOf(price) };
				if (genre.toLowerCase().contains(searchedMovie.toLowerCase())) {
					adminView.insertMovie(row);
				}
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public void searchRomance() {
		String searchedMovie = "Romance";
		repository = new Repository(new Movie());
		ResultSet rez = repository.findAll();
		adminView.clearMoviesTable();
		
		try {
			while(rez.next()) {
				int id = rez.getInt("id");
				String name = rez.getString("name");
				String regizor = rez.getString("regizor");
				String description = rez.getString("description");
				Double rating = rez.getDouble("rating");
				String genre = rez.getString("genre");
				int quantity = rez.getInt("quantity");
				Double price = rez.getDouble("price");
				String[] row = { String.valueOf(id), name, regizor, description, String.valueOf(rating), genre, String.valueOf(quantity), String.valueOf(price) };
				if (genre.toLowerCase().contains(searchedMovie.toLowerCase())) {
					adminView.insertMovie(row);
				}
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public void searchUser() {
		adminView.clearUsersTable();
		int id = adminView.getSearchedUser();
		repository = new Repository(new User());
		ResultSet rez = repository.findById(id);
		adminView.clearMoviesTable();
		
		try {
			while(rez.next()) {
			String name = rez.getString("name");
			String cnp = rez.getString("cnp");
			String address = rez.getString("address");
			String username = rez.getString("username");
			String password = rez.getString("password");
			String type = rez.getString("type");
			String[] row = { String.valueOf(id), name, cnp, address, username, password, type };
			adminView.insertUser(row);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void searchOrder() {
		adminView.clearOrdersTable();
		int id = adminView.getSearchedOrder();
		repository = new Repository(new Order());
		ResultSet rez = repository.findById(id);
		
		try {
			while(rez.next()) {
				int userId = rez.getInt("userId");
				int movieId = rez.getInt("movieId");
				int quantity = rez.getInt("quantity");
				Double price = rez.getDouble("price");
				Date date = rez.getDate("date");
				String status = rez.getString("status");
				String[] row = { String.valueOf(id), String.valueOf(userId), String.valueOf(movieId), String.valueOf(quantity), String.valueOf(price), String.valueOf(date), status };
				adminView.insertOrder(row);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void updateOrderStatus() {
		adminView.clearOrdersTable();
		Order order = adminView.getOrderToUpdate();
		String status = adminView.getNewOrderStatus();
		order.setStatus(status);
		System.out.println(order.getId()+order.getStatus());
		repository = new Repository(order);
		repository.update();
		adminView.clearOrdersTable();
		loadAllOrders();
	}
	
	public void logout() {
		adminView.dispose();
	}
	
}
