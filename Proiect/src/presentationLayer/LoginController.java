package presentationLayer;

import businessLayer.Login;
import model.User;

public class LoginController {
	private User currentUser;
	private LoginView loginView;
	private Login login;
	
	public LoginController() {
		
	}
	
	public LoginController(LoginView loginView) {
		this.loginView = loginView;
		this.login = new Login();
	}
	
	public void loginButtonClicked() {
		String username = loginView.getUsername();
		String password = loginView.getPassword();
		if(username.isEmpty() || password.isEmpty())
			loginView.displayMessage("Introduceti username ul si parola.", "", 1);
		else {
			currentUser = login.login(username, password);
			if(currentUser.getId() > 0) {
				if(currentUser.getType().equals("Admin")) {
					AdminView view = new AdminView(currentUser);
				} else {
					UserView view = new UserView(currentUser);
				}
			} else {
				loginView.displayMessage("Combinatie username/parola invalide.", "", 1);
			}
		}
	}
}
