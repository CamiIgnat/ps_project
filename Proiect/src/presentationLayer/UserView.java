package presentationLayer;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import model.Movie;
import model.User;

public class UserView extends JFrame {
	private JPanel Panel;
	private JTable moviesTable;
	private DefaultTableModel moviesDTM;
	private JScrollPane moviesScroll;
	
	private JLabel searchLabel;
	private JTextField searchTF;
	private JButton searchByTitle;
	private JButton searchByAuthor;
	
	private JLabel sellLabel;
	private JTextField sellTF;
	private JButton sellButton;
	
	private User currentUser;
	
	private JLabel categoriesLabel;
	private JButton dramaButton;
	private JButton comedyButton;
	private JButton horrorButton;
	private JButton actionButton;
	private JButton romanceButton;
	private JButton allMoviesButton;
	
	private JLabel ratingLabel;
	private JTextField ratingTextField;
	private JButton ratingButton;
	
	private Movie selectedMovie;
	private JTabbedPane tabbedPane;
	
	private JPanel ordersPanel;
	private JTable ordersTable;
	private DefaultTableModel ordersDTM;
	private JScrollPane ordersScroll;
	
	private JButton logoutButton;
	private JButton logoutB;
	
	public UserView(User currentUser){
		this.currentUser = currentUser;
		setupAppearance();
		
		UserController userController = new UserController(this);
		
		
		searchByTitle.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				userController.searchMovieByTitle();
				
			}
		});
		
		
		searchByAuthor.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				userController.searchMovieByRegizor();
				
			}
		});
		
		
		sellButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				selectedMovie = getSelectedMovie();
				userController.sellMovie();
				
			}
		});
		
		ratingButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				selectedMovie = getSelectedMovie();
				userController.giveRating();
				
			}
		});
		
		dramaButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				userController.searchDrama();
				
			}
		});
		
		comedyButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				userController.searchComedy();
				
			}
		});
		
		horrorButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				userController.searchHorror();
				
			}
		});
		
		actionButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				userController.searchAction();
				
			}
		});
		
		romanceButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				userController.searchRomance();
				
			}
		});
		
		allMoviesButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				userController.loadAllMovies();
				
			}
		});
		
		logoutButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				userController.logout();
				
			}
		});
		
		logoutB.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				userController.logout();
				
			}
		});
		
		userController.loadAllMovies();
		userController.loadAllOrders();
		
		this.setContentPane(tabbedPane);
		this.setVisible(true);
		this.setResizable(false);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setBounds(60,30,1300,600);
		this.setTitle("");
	}
	
	public void setupAppearance() {
		tabbedPane = new JTabbedPane();
		
		Panel = new JPanel();
		Panel.setLayout(null);
		
		ordersPanel = new JPanel();
		ordersPanel.setLayout(null);
		
		tabbedPane.add("Movies",Panel);
		tabbedPane.add("Orders",ordersPanel);
		
		moviesDTM =new DefaultTableModel();
		moviesDTM.addColumn("Id");
		moviesDTM.addColumn("Title");
		moviesDTM.addColumn("Regizor");
		moviesDTM.addColumn("Description");
		moviesDTM.addColumn("Rating");
		moviesDTM.addColumn("Genre");
		moviesDTM.addColumn("Quantity");
		moviesDTM.addColumn("Price");
		moviesTable = new JTable(moviesDTM);
		moviesTable.setBackground(Color.lightGray);
		moviesTable.setOpaque(true);
		moviesTable.setFont(new Font("Serif", Font.PLAIN, 15));
		moviesTable.setRowHeight(20);
		moviesScroll = new JScrollPane(moviesTable);
		moviesScroll.setBounds(10,60,900,500);
		Panel.add(moviesScroll);
		
		searchLabel = new JLabel("Search:");
		searchLabel.setFont(new Font("Grill Sans MT", Font.BOLD, 12));
		searchLabel.setBounds(930, 50, 100, 22);
		Panel.add(searchLabel);
		
		searchTF = new JTextField();
		searchTF.setBounds(940, 80, 150, 22);
		Panel.add(searchTF);
		
		searchByTitle = new JButton("By title");
		searchByTitle.setBounds(940, 110, 150, 30);
		Panel.add(searchByTitle);
		
		searchByAuthor = new JButton("By author");
		searchByAuthor.setBounds(1100, 110, 150, 30);
		Panel.add(searchByAuthor);

		
		sellLabel = new JLabel("Movies quantity:");
		sellLabel.setFont(new Font("Grill Sans MT", Font.BOLD, 12));
		sellLabel.setBounds(930, 250, 100, 22);
		Panel.add(sellLabel);
		
		sellTF = new JTextField();
		sellTF.setBounds(940, 280, 150, 22);
		Panel.add(sellTF);
		
		sellButton = new JButton("Buy");
		sellButton.setBounds(940, 310, 150, 30);
		Panel.add(sellButton);
		
		ratingLabel = new JLabel("Rating(1-5):");
		ratingLabel.setFont(new Font("Grill Sans MT", Font.BOLD, 12));
		ratingLabel.setBounds(930, 370, 100, 22);
		Panel.add(ratingLabel);
		
		ratingTextField =  new JTextField();
		ratingTextField.setBounds(940, 400, 150, 22);
		Panel.add(ratingTextField);
		
		ratingButton = new JButton("Rate");
		ratingButton.setBounds(940, 430, 150, 30);
		Panel.add(ratingButton);
		
		categoriesLabel = new JLabel("Category:");
		categoriesLabel.setFont(new Font("Grill Sans MT", Font.BOLD, 12));
		categoriesLabel.setBounds(10, 10, 100, 30);
		Panel.add(categoriesLabel);
		
		actionButton = new JButton("Action");
		actionButton.setBounds(120, 10, 100, 30);
		Panel.add(actionButton);
		
		comedyButton = new JButton("Comedy");
		comedyButton.setBounds(230, 10, 100, 30);
		Panel.add(comedyButton);
		
		dramaButton = new JButton("Drama");
		dramaButton.setBounds(340, 10, 100, 30);
		Panel.add(dramaButton);
		
		horrorButton = new JButton("Horror");
		horrorButton.setBounds(450, 10, 100, 30);
		Panel.add(horrorButton);
		
		romanceButton = new JButton("Romance");
		romanceButton.setBounds(560, 10, 100, 30);
		Panel.add(romanceButton);
		
		allMoviesButton = new JButton("All movies");
		allMoviesButton.setBounds(790, 10, 100, 30);
		Panel.add(allMoviesButton);
		
		
		ordersDTM =new DefaultTableModel();
		ordersDTM.addColumn("Id");
		ordersDTM.addColumn("UserId");
		ordersDTM.addColumn("movieId");
		ordersDTM.addColumn("Quantity");
		ordersDTM.addColumn("Price");
		ordersDTM.addColumn("Date");
		ordersDTM.addColumn("Status");
		ordersTable = new JTable(ordersDTM);
		ordersTable.setBackground(Color.lightGray);
		ordersTable.setOpaque(true);
		ordersTable.setFont(new Font("Serif", Font.PLAIN, 15));
		ordersTable.setRowHeight(20);
		ordersScroll = new JScrollPane(ordersTable);
		ordersScroll.setBounds(10,60,900,500);
		ordersPanel.add(ordersScroll);
		
		logoutButton = new JButton("Logout");
		logoutButton.setBounds(1100, 10, 150, 30);
		logoutButton.setForeground(Color.white);
		logoutButton.setBackground(Color.darkGray);
		Panel.add(logoutButton);
		
		logoutB = new JButton("Logout");
		logoutB.setBounds(1100, 10, 150, 30);
		logoutB.setForeground(Color.white);
		logoutB.setBackground(Color.darkGray);
		ordersPanel.add(logoutB);
		
		
		
	}
	
	public void insertBook(String[] row) {
		moviesDTM.addRow(row);
	}
	
	public void clearBooksTable() {
		for(int i=0;i< moviesDTM.getRowCount(); i++)
			moviesDTM.removeRow(i);
		moviesDTM.setRowCount(0);
	}
	
	public String getSearchedBook() {
		return searchTF.getText();
	}
	
	public Movie getSelectedMovie() {
		Movie movie = new Movie();
		int row = moviesTable.getSelectedRow();
		if(row>=0) {
			movie.setId(Integer.parseInt(moviesDTM.getValueAt(row, 0).toString()));
			movie.setName(moviesDTM.getValueAt(row, 1).toString());
			movie.setRegizor(moviesDTM.getValueAt(row, 2).toString());
			movie.setDescription(moviesDTM.getValueAt(row, 3).toString());
			movie.setRating(Double.valueOf(moviesDTM.getValueAt(row, 4).toString()));
			movie.setGenre(moviesDTM.getValueAt(row, 5).toString());
			movie.setQuantity(Integer.parseInt(moviesDTM.getValueAt(row, 6).toString()));
			movie.setPrice(Double.valueOf(moviesDTM.getValueAt(row, 7).toString()));
		}
		moviesTable.getSelectionModel().clearSelection();
		
		return movie;
	}
	
	public int getQuantity() {
		return Integer.parseInt(sellTF.getText());
	}
	
	public User getCurrentUser() {
		return currentUser;
	}
	
	public void insertMovie(String[] row) {
		moviesDTM.addRow(row);
	}
	
	public void clearMoviesTable() {
		for(int i=0; i < moviesDTM.getRowCount(); i++)
			moviesDTM.removeRow(i);
		moviesDTM.setRowCount(0);
	}
	
	public String getSearchedMovie() {
		return searchTF.getText();
	}
	
	public Movie getSellMovie() {
		return selectedMovie;
	}
	
	public int getMovieQuantity() {
		return Integer.parseInt(sellTF.getText());
	}
	
	public double getRating() {
		return Double.parseDouble(ratingTextField.getText());
	}
	
	public void clearOrdersTable() {
		for(int i=0; i < ordersDTM.getRowCount(); i++)
			ordersDTM.removeRow(i);
		ordersDTM.setRowCount(0);
	}
	
	public void insertOrder(String[] row) {
		ordersDTM.addRow(row);
	}
}
