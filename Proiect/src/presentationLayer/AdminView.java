package presentationLayer;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import model.Movie;
import model.Order;
import model.User;

public class AdminView extends JFrame {
	private JTabbedPane tabbedPane;
	
	private JPanel Panel;
	private JTable moviesTable;
	private DefaultTableModel moviesDTM;
	private JScrollPane moviesScroll;
	
	private JLabel searchLabel;
	private JTextField searchTF;
	private JButton searchByTitle;
	private JButton searchByAuthor;
	
	
	private User currentUser;
	
	private JLabel categoriesLabel;
	private JButton dramaButton;
	private JButton comedyButton;
	private JButton horrorButton;
	private JButton actionButton;
	private JButton romanceButton;
	private JButton allMoviesButton;
	
	private JButton createMovie;
	private JButton updateMovie;
	private JButton deleteMovie;
	
	private JDialog createMovieDialog;
	private JLabel createMovieNameLabel;
	private JTextField createMovieNameTF;
	private JLabel createRegizorLabel;
	private JTextField createRegizorTF;
	private JLabel createDescriptionLabel;
	private JTextField createDescriptionTF;
	private JLabel createRatingLabel;
	private JTextField createRatingTF;
	private JLabel createGenreLabel;
	private JTextField createGenreTF;
	private JLabel createQuantityLabel;
	private JTextField createQuantityTF;
	private JLabel createPriceLabel;
	private JTextField createPriceTF;
	private JButton createMovieButton;
	
	private JDialog updateMovieDialog;
	private JLabel updateMovieNameLabel;
	private JTextField updateMovieNameTF;
	private JLabel updateRegizorLabel;
	private JTextField updateRegizorTF;
	private JLabel updateDescriptionLabel;
	private JTextField updateDescriptionTF;
	private JLabel updateRatingLabel;
	private JTextField updateRatingTF;
	private JLabel updateGenreLabel;
	private JTextField updateGenreTF;
	private JLabel updateQuantityLabel;
	private JTextField updateQuantityTF;
	private JLabel updatePriceLabel;
	private JTextField updatePriceTF;
	private JButton updateMovieButton;
	
	///////////////////////////////////////////////////////////
	private JPanel usersPanel;
	private JTable usersTable;
	private DefaultTableModel usersDTM;
	private JScrollPane usersScroll;
	
	private JButton createUser;
	private JButton updateUser;
	private JButton deleteUser;
	private JLabel searchUserLabel;
	private JTextField searchUserTF;
	private JButton searchUserButton;
	
	private JDialog createUserDialog;
	private JLabel createNameLabel;
	private JTextField createNameTF;
	private JLabel createCNPLabel;
	private JTextField createCNPTF;
	private JLabel createAddressLabel;
	private JTextField createAddressTF;
	private JLabel createUsernameLabel;
	private JTextField createUsernameTF;
	private JLabel createPasswordLabel;
	private JTextField createPasswordTF;
	private JLabel createTypeLabel;
	private JTextField createTypeTF;
	private JButton createUserButton;
	
	private JDialog updateUserDialog;
	private JLabel updateNameLabel;
	private JTextField updateNameTF;
	private JLabel updateCNPLabel;
	private JTextField updateCNPTF;
	private JLabel updateAddressLabel;
	private JTextField updateAddressTF;
	private JLabel updateUsernameLabel;
	private JTextField updateUsernameTF;
	private JLabel updatePasswordLabel;
	private JTextField updatePasswordTF;
	private JLabel updateTypeLabel;
	private JTextField updateTypeTF;
	private JButton updateUserButton;

	
	////////////////////////////////////////////////////////////
	private JPanel ordersPanel;
	private JTable ordersTable;
	private DefaultTableModel ordersDTM;
	private JScrollPane ordersScroll;
	
	private JLabel searchOrderLabel;
	private JTextField searchOrderTF;
	private JButton searchOrderButton;
	private JLabel changeStatusOrderLabel;
	private JTextField changeStatusOrderTF;
	private JButton changeStatusOrderButton;
	
	/////////
	private User selectedUser;
	private Movie selectedMovie;
	private Order selectedOrder;
	
	private JButton logoutButton1;
	private JButton logoutButton2;
	private JButton logoutButton3;
	
	public AdminView(User currentUser){
		this.currentUser = currentUser;
		setupAppearance();
		
		AdminController adminController = new AdminController(this);
		
		
		searchByTitle.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				adminController.searchMovieByTitle();
				
			}
		});
		
		
		searchByAuthor.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				adminController.searchMovieByRegizor();
				
			}
		});
		
		createMovie.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				createMovieDialog.setVisible(true);
				
			}
		});
		
		updateMovie.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				updateMovieDialog.setVisible(true);
				selectedMovie = getSelectedMovie();
				
			}
		});
		
		deleteMovie.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				selectedMovie = getSelectedMovie();
				adminController.deleteMovie();
				
			}
		});
		
		createMovieButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				adminController.addMovie();
				
			}
		});
		
		updateMovieButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				adminController.updateMovie();
				
			}
		});
	
		
		dramaButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				adminController.searchDrama();
				
			}
		});
		comedyButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				adminController.searchComedy();
				
			}
		});
		
		horrorButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				adminController.searchHorror();
				
			}
		});

		actionButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				adminController.searchAction();
				
			}
		});
		
		romanceButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				adminController.searchRomance();
				
			}
		});
		
		allMoviesButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				adminController.loadAllMovies();
				
			}
		});
		
		//////////////////////////////////////////////////////////////////////////////
		
		createUser.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				createUserDialog.setVisible(true);
				
			}
		});

		updateUser.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				updateUserDialog.setVisible(true);
				selectedUser = getSelectedUser();
				
			}
		});
		
		deleteUser.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				selectedUser = getSelectedUser();
				adminController.deleteUser();
				
			}
		});
		
		searchUserButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				adminController.searchUser();
				
			}
		});
		
		createUserButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				adminController.addUser();
				
			}
		});
		
		updateUserButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				adminController.updateUser();
				
			}
		});
		////////////////////////////////////////////////////////////////////////////
		searchOrderButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				adminController.searchOrder();
				
			}
		});
		
		changeStatusOrderButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				selectedOrder = getSelectedOrder();
				adminController.updateOrderStatus();
				
			}
		});
		
		logoutButton1.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				adminController.logout();
				
			}
		});
		
		logoutButton2.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				adminController.logout();
				
			}
		});
		
		logoutButton3.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				adminController.logout();
				
			}
		});
		
		adminController.loadAllMovies();
		adminController.loadAllOrders();
		adminController.loadAllUsers();
		
		
		this.setContentPane(tabbedPane);
		this.setVisible(true);
		this.setResizable(false);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setBounds(60,30,1300,600);
		this.setTitle("");
	}
	
	public void setupAppearance() {
		tabbedPane = new JTabbedPane();
		
		Panel = new JPanel();
		Panel.setLayout(null);
		
		usersPanel = new JPanel();
		usersPanel.setLayout(null);
		
		ordersPanel = new JPanel();
		ordersPanel.setLayout(null);
		
		tabbedPane.add("Movies",Panel);
		tabbedPane.add("Users",usersPanel);
		tabbedPane.add("Orders",ordersPanel);
		
		moviesDTM =new DefaultTableModel();
		moviesDTM.addColumn("Id");
		moviesDTM.addColumn("Title");
		moviesDTM.addColumn("Regizor");
		moviesDTM.addColumn("Description");
		moviesDTM.addColumn("Rating");
		moviesDTM.addColumn("Genre");
		moviesDTM.addColumn("Quantity");
		moviesDTM.addColumn("Price");
		moviesTable = new JTable(moviesDTM);
		moviesTable.setBackground(Color.lightGray);
		moviesTable.setOpaque(true);
		moviesTable.setFont(new Font("Serif", Font.PLAIN, 15));
		moviesTable.setRowHeight(20);
		moviesScroll = new JScrollPane(moviesTable);
		moviesScroll.setBounds(10,60,900,500);
		Panel.add(moviesScroll);
		
		searchLabel = new JLabel("Search:");
		searchLabel.setFont(new Font("Grill Sans MT", Font.BOLD, 12));
		searchLabel.setBounds(930, 50, 100, 22);
		Panel.add(searchLabel);
		
		searchTF = new JTextField();
		searchTF.setBounds(940, 80, 150, 22);
		Panel.add(searchTF);
		
		searchByTitle = new JButton("By name");
		searchByTitle.setBounds(940, 110, 150, 30);
		Panel.add(searchByTitle);
		
		searchByAuthor = new JButton("By director");
		searchByAuthor.setBounds(1100, 110, 150, 30);
		Panel.add(searchByAuthor);
		
		createMovie = new JButton("Create");
		createMovie.setBounds(940, 300, 150, 30);
		Panel.add(createMovie);
		
		updateMovie = new JButton("Update");
		updateMovie.setBounds(940, 340, 150, 30);
		Panel.add(updateMovie);
		
		deleteMovie = new JButton("Delete");
		deleteMovie.setBounds(940, 380, 150, 30);
		Panel.add(deleteMovie);
		
		categoriesLabel = new JLabel("Category:");
		categoriesLabel.setFont(new Font("Grill Sans MT", Font.BOLD, 12));
		categoriesLabel.setBounds(10, 10, 100, 30);
		Panel.add(categoriesLabel);
		
		actionButton = new JButton("Action");
		actionButton.setBounds(120, 10, 100, 30);
		Panel.add(actionButton);
		
		comedyButton = new JButton("Comedy");
		comedyButton.setBounds(230, 10, 100, 30);
		Panel.add(comedyButton);
		
		dramaButton = new JButton("Drama");
		dramaButton.setBounds(340, 10, 100, 30);
		Panel.add(dramaButton);
		
		horrorButton = new JButton("Horror");
		horrorButton.setBounds(450, 10, 100, 30);
		Panel.add(horrorButton);
		
		romanceButton = new JButton("Romance");
		romanceButton.setBounds(560, 10, 100, 30);
		Panel.add(romanceButton);
		
		allMoviesButton = new JButton("All movies");
		allMoviesButton.setBounds(790, 10, 100, 30);
		Panel.add(allMoviesButton);
		
		createMovieDialog = new JDialog(this,"Create");
		createMovieDialog.setSize(500, 300);
		createMovieDialog.setLocation(200, 200);
		createMovieDialog.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		createMovieDialog.setVisible(false);
		createMovieDialog.setLayout(null);
		
		
		createMovieNameLabel = new JLabel("Name:");
		createMovieNameLabel.setFont(new Font("Grill Sans MT", Font.BOLD, 12));
		createMovieNameLabel.setBounds(100, 10, 100, 22);
		createMovieDialog.add(createMovieNameLabel);
		
		createMovieNameTF = new JTextField();
		createMovieNameTF.setBounds(180, 10, 150, 22);
		createMovieDialog.add(createMovieNameTF);
		
		createRegizorLabel = new JLabel("Regizor:");
		createRegizorLabel.setFont(new Font("Grill Sans MT", Font.BOLD, 12));
		createRegizorLabel.setBounds(100, 40, 100, 22);
		createMovieDialog.add(createRegizorLabel);
		
		createRegizorTF = new JTextField();
		createRegizorTF.setBounds(180, 40, 150, 22);
		createMovieDialog.add(createRegizorTF);
		
		createDescriptionLabel = new JLabel("Details:");
		createDescriptionLabel.setFont(new Font("Grill Sans MT", Font.BOLD, 12));
		createDescriptionLabel.setBounds(100, 70, 100, 22);
		createMovieDialog.add(createDescriptionLabel);
		
		createDescriptionTF = new JTextField();
		createDescriptionTF.setBounds(180, 70, 150, 22);
		createMovieDialog.add(createDescriptionTF);
		
		createRatingLabel = new JLabel("Rating:");
		createRatingLabel.setFont(new Font("Grill Sans MT", Font.BOLD, 12));
		createRatingLabel.setBounds(100, 100, 100, 22);
		createMovieDialog.add(createRatingLabel);
		
		createRatingTF = new JTextField();
		createRatingTF.setBounds(180, 100, 150, 22);
		createMovieDialog.add(createRatingTF);
		
		createGenreLabel = new JLabel("Genre:");
		createGenreLabel.setFont(new Font("Grill Sans MT", Font.BOLD, 12));
		createGenreLabel.setBounds(100, 130, 100, 22);
		createMovieDialog.add(createGenreLabel);
		
		createGenreTF = new JTextField();
		createGenreTF.setBounds(180, 130, 150, 22);
		createMovieDialog.add(createGenreTF);
		
		createQuantityLabel = new JLabel("Quantity:");
		createQuantityLabel.setFont(new Font("Grill Sans MT", Font.BOLD, 12));
		createQuantityLabel.setBounds(100, 160, 100, 22);
		createMovieDialog.add(createQuantityLabel);
		
		createQuantityTF = new JTextField();
		createQuantityTF.setBounds(180, 160, 150, 22);
		createMovieDialog.add(createQuantityTF);
		
		createPriceLabel = new JLabel("Price:");
		createPriceLabel.setFont(new Font("Grill Sans MT", Font.BOLD, 12));
		createPriceLabel.setBounds(100, 190, 100, 22);
		createMovieDialog.add(createPriceLabel);
		
		createPriceTF = new JTextField();
		createPriceTF.setBounds(180, 190, 150, 22);
		createMovieDialog.add(createPriceTF);
		
		createMovieButton = new JButton("Create");
		createMovieButton.setBounds(180, 220, 150, 30);
		createMovieDialog.add(createMovieButton);
		
		
		updateMovieDialog = new JDialog(this,"Update");
		updateMovieDialog.setSize(500, 300);
		updateMovieDialog.setLocation(200, 200);
		updateMovieDialog.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		updateMovieDialog.setVisible(false);
		updateMovieDialog.setLayout(null);
		
		
		updateMovieNameLabel = new JLabel("Name:");
		updateMovieNameLabel.setFont(new Font("Grill Sans MT", Font.BOLD, 12));
		updateMovieNameLabel.setBounds(100, 10, 100, 22);
		updateMovieDialog.add(updateMovieNameLabel);
		
		updateMovieNameTF = new JTextField();
		updateMovieNameTF.setBounds(180, 10, 150, 22);
		updateMovieDialog.add(updateMovieNameTF);
		
		updateRegizorLabel = new JLabel("Regizor:");
		updateRegizorLabel.setFont(new Font("Grill Sans MT", Font.BOLD, 12));
		updateRegizorLabel.setBounds(100, 40, 100, 22);
		updateMovieDialog.add(updateRegizorLabel);
		
		updateRegizorTF = new JTextField();
		updateRegizorTF.setBounds(180, 40, 150, 22);
		updateMovieDialog.add(updateRegizorTF);
		
		updateDescriptionLabel = new JLabel("Details:");
		updateDescriptionLabel.setFont(new Font("Grill Sans MT", Font.BOLD, 12));
		updateDescriptionLabel.setBounds(100, 70, 100, 22);
		updateMovieDialog.add(updateDescriptionLabel);
		
		updateDescriptionTF = new JTextField();
		updateDescriptionTF.setBounds(180, 70, 150, 22);
		updateMovieDialog.add(updateDescriptionTF);
		
		updateRatingLabel = new JLabel("Rating:");
		updateRatingLabel.setFont(new Font("Grill Sans MT", Font.BOLD, 12));
		updateRatingLabel.setBounds(100, 100, 100, 22);
		updateMovieDialog.add(updateRatingLabel);
		
		updateRatingTF = new JTextField();
		updateRatingTF.setBounds(180, 100, 150, 22);
		updateMovieDialog.add(updateRatingTF);
		
		updateGenreLabel = new JLabel("Genre:");
		updateGenreLabel.setFont(new Font("Grill Sans MT", Font.BOLD, 12));
		updateGenreLabel.setBounds(100, 130, 100, 22);
		updateMovieDialog.add(updateGenreLabel);
		
		updateGenreTF = new JTextField();
		updateGenreTF.setBounds(180, 130, 150, 22);
		updateMovieDialog.add(updateGenreTF);
		
		updateQuantityLabel = new JLabel("Quantity:");
		updateQuantityLabel.setFont(new Font("Grill Sans MT", Font.BOLD, 12));
		updateQuantityLabel.setBounds(100, 160, 100, 22);
		updateMovieDialog.add(updateQuantityLabel);
		
		updateQuantityTF = new JTextField();
		updateQuantityTF.setBounds(180, 160, 150, 22);
		updateMovieDialog.add(updateQuantityTF);
		
		updatePriceLabel = new JLabel("Price:");
		updatePriceLabel.setFont(new Font("Grill Sans MT", Font.BOLD, 12));
		updatePriceLabel.setBounds(100, 190, 100, 22);
		updateMovieDialog.add(updatePriceLabel);
		
		updatePriceTF = new JTextField();
		updatePriceTF.setBounds(180, 190, 150, 22);
		updateMovieDialog.add(updatePriceTF);
		
		updateMovieButton = new JButton("Update");
		updateMovieButton.setBounds(180, 220, 150, 30);
		updateMovieDialog.add(updateMovieButton);
		
		////////////////////////////////////////////////////////////////////////////////////
		
		usersDTM =new DefaultTableModel();
		usersDTM.addColumn("Id");
		usersDTM.addColumn("Name");
		usersDTM.addColumn("CNP");
		usersDTM.addColumn("Address");
		usersDTM.addColumn("Username");
		usersDTM.addColumn("Password");
		usersDTM.addColumn("Type");
		usersTable = new JTable(usersDTM);
		usersTable.setBackground(Color.lightGray);
		usersTable.setOpaque(true);
		usersTable.setFont(new Font("Serif", Font.PLAIN, 15));
		usersTable.setRowHeight(20);
		usersScroll = new JScrollPane(usersTable);
		usersScroll.setBounds(10,60,900,500);
		usersPanel.add(usersScroll);
		
		searchUserLabel = new JLabel("Search user by id:");
		searchUserLabel.setFont(new Font("Grill Sans MT", Font.BOLD, 12));
		searchUserLabel.setBounds(930, 100, 150, 22);
		usersPanel.add(searchUserLabel);
		
		searchUserTF = new JTextField();
		searchUserTF.setBounds(940, 130, 150, 22);
		usersPanel.add(searchUserTF);
		
		searchUserButton = new JButton("Search");
		searchUserButton.setBounds(940, 160, 150, 30);
		usersPanel.add(searchUserButton);
		
		createUser = new JButton("Create");
		createUser.setBounds(940, 300, 150, 30);
		usersPanel.add(createUser);
		
		updateUser = new JButton("Update");
		updateUser.setBounds(940, 340, 150, 30);
		usersPanel.add(updateUser);
		
		deleteUser = new JButton("Delete");
		deleteUser.setBounds(940, 380, 150, 30);
		usersPanel.add(deleteUser);
		
		createUserDialog = new JDialog(this,"Create");
		createUserDialog.setSize(500, 300);
		createUserDialog.setLocation(200, 200);
		createUserDialog.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		createUserDialog.setVisible(false);
		createUserDialog.setLayout(null);
		
		
		createNameLabel = new JLabel("Name:");
		createNameLabel.setFont(new Font("Grill Sans MT", Font.BOLD, 12));
		createNameLabel.setBounds(100, 10, 100, 22);
		createUserDialog.add(createNameLabel);
		
		createNameTF = new JTextField();
		createNameTF.setBounds(180, 10, 150, 22);
		createUserDialog.add(createNameTF);
		
		createCNPLabel = new JLabel("CNP:");
		createCNPLabel.setFont(new Font("Grill Sans MT", Font.BOLD, 12));
		createCNPLabel.setBounds(100, 40, 100, 22);
		createUserDialog.add(createCNPLabel);
		
		createCNPTF = new JTextField();
		createCNPTF.setBounds(180, 40, 150, 22);
		createUserDialog.add(createCNPTF);
		
		createAddressLabel = new JLabel("Address:");
		createAddressLabel.setFont(new Font("Grill Sans MT", Font.BOLD, 12));
		createAddressLabel.setBounds(100, 70, 100, 22);
		createUserDialog.add(createAddressLabel);
		
		createAddressTF = new JTextField();
		createAddressTF.setBounds(180, 70, 150, 22);
		createUserDialog.add(createAddressTF);
		
		createUsernameLabel = new JLabel("Username:");
		createUsernameLabel.setFont(new Font("Grill Sans MT", Font.BOLD, 12));
		createUsernameLabel.setBounds(100, 100, 100, 22);
		createUserDialog.add(createUsernameLabel);
		
		createUsernameTF = new JTextField();
		createUsernameTF.setBounds(180, 100, 150, 22);
		createUserDialog.add(createUsernameTF);
		
		createPasswordLabel = new JLabel("Password:");
		createPasswordLabel.setFont(new Font("Grill Sans MT", Font.BOLD, 12));
		createPasswordLabel.setBounds(100, 130, 100, 22);
		createUserDialog.add(createPasswordLabel);
		
		createPasswordTF = new JTextField();
		createPasswordTF.setBounds(180, 130, 150, 22);
		createUserDialog.add(createPasswordTF);
		
		createTypeLabel = new JLabel("Type:");
		createTypeLabel.setFont(new Font("Grill Sans MT", Font.BOLD, 12));
		createTypeLabel.setBounds(100, 160, 100, 22);
		createUserDialog.add(createTypeLabel);
		
		createTypeTF = new JTextField();
		createTypeTF.setBounds(180, 160, 150, 22);
		createUserDialog.add(createTypeTF);
		
		createUserButton = new JButton("Create");
		createUserButton.setBounds(180, 210, 150, 30);
		createUserDialog.add(createUserButton);
		
		
		updateUserDialog = new JDialog(this,"Update");
		updateUserDialog.setSize(500, 300);
		updateUserDialog.setLocation(200, 200);
		updateUserDialog.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		updateUserDialog.setVisible(false);
		updateUserDialog.setLayout(null);
		
		
		updateNameLabel = new JLabel("Name:");
		updateNameLabel.setFont(new Font("Grill Sans MT", Font.BOLD, 12));
		updateNameLabel.setBounds(100, 10, 100, 22);
		updateUserDialog.add(updateNameLabel);
		
		updateNameTF = new JTextField();
		updateNameTF.setBounds(180, 10, 150, 22);
		updateUserDialog.add(updateNameTF);
		
		updateCNPLabel = new JLabel("CNP:");
		updateCNPLabel.setFont(new Font("Grill Sans MT", Font.BOLD, 12));
		updateCNPLabel.setBounds(100, 40, 100, 22);
		updateUserDialog.add(updateCNPLabel);
		
		updateCNPTF = new JTextField();
		updateCNPTF.setBounds(180, 40, 150, 22);
		updateUserDialog.add(updateCNPTF);
		
		updateAddressLabel = new JLabel("Address:");
		updateAddressLabel.setFont(new Font("Grill Sans MT", Font.BOLD, 12));
		updateAddressLabel.setBounds(100, 70, 100, 22);
		updateUserDialog.add(updateAddressLabel);
		
		updateAddressTF = new JTextField();
		updateAddressTF.setBounds(180, 70, 150, 22);
		updateUserDialog.add(updateAddressTF);
		
		updateUsernameLabel = new JLabel("Username:");
		updateUsernameLabel.setFont(new Font("Grill Sans MT", Font.BOLD, 12));
		updateUsernameLabel.setBounds(100, 100, 100, 22);
		updateUserDialog.add(updateUsernameLabel);
		
		updateUsernameTF = new JTextField();
		updateUsernameTF.setBounds(180, 100, 150, 22);
		updateUserDialog.add(updateUsernameTF);
		
		updatePasswordLabel = new JLabel("Password:");
		updatePasswordLabel.setFont(new Font("Grill Sans MT", Font.BOLD, 12));
		updatePasswordLabel.setBounds(100, 130, 100, 22);
		updateUserDialog.add(updatePasswordLabel);
		
		updatePasswordTF = new JTextField();
		updatePasswordTF.setBounds(180, 130, 150, 22);
		updateUserDialog.add(updatePasswordTF);
		
		updateTypeLabel = new JLabel("Type:");
		updateTypeLabel.setFont(new Font("Grill Sans MT", Font.BOLD, 12));
		updateTypeLabel.setBounds(100, 160, 100, 22);
		updateUserDialog.add(updateTypeLabel);
		
		updateTypeTF = new JTextField();
		updateTypeTF.setBounds(180, 160, 150, 22);
		updateUserDialog.add(updateTypeTF);
		
		updateUserButton = new JButton("Update");
		updateUserButton.setBounds(180, 210, 150, 30);
		updateUserDialog.add(updateUserButton);
		
		//////////////////////////////////////////////////////////////////////////////
		ordersDTM =new DefaultTableModel();
		ordersDTM.addColumn("Id");
		ordersDTM.addColumn("UserId");
		ordersDTM.addColumn("movieId");
		ordersDTM.addColumn("Quantity");
		ordersDTM.addColumn("Price");
		ordersDTM.addColumn("Date");
		ordersDTM.addColumn("Status");
		ordersTable = new JTable(ordersDTM);
		ordersTable.setBackground(Color.lightGray);
		ordersTable.setOpaque(true);
		ordersTable.setFont(new Font("Serif", Font.PLAIN, 15));
		ordersTable.setRowHeight(20);
		ordersScroll = new JScrollPane(ordersTable);
		ordersScroll.setBounds(10,60,900,500);
		ordersPanel.add(ordersScroll);
		
		searchOrderLabel = new JLabel("Search order by id:");
		searchOrderLabel.setFont(new Font("Grill Sans MT", Font.BOLD, 12));
		searchOrderLabel.setBounds(930, 100, 150, 22);
		ordersPanel.add(searchOrderLabel);
		
		searchOrderTF = new JTextField();
		searchOrderTF.setBounds(940, 130, 150, 22);
		ordersPanel.add(searchOrderTF);
		
		searchOrderButton = new JButton("Search");
		searchOrderButton.setBounds(940, 160, 150, 30);
		ordersPanel.add(searchOrderButton);
		
		changeStatusOrderLabel = new JLabel("Change order status:");
		changeStatusOrderLabel.setFont(new Font("Grill Sans MT", Font.BOLD, 12));
		changeStatusOrderLabel.setBounds(930, 250, 150, 22);
		ordersPanel.add(changeStatusOrderLabel);
		
		changeStatusOrderTF = new JTextField();
		changeStatusOrderTF.setBounds(940, 280, 150, 22);
		ordersPanel.add(changeStatusOrderTF);
		
		changeStatusOrderButton = new JButton("Change");
		changeStatusOrderButton.setBounds(940, 310, 150, 30);
		ordersPanel.add(changeStatusOrderButton);
		
		logoutButton1 = new JButton("Logout");
		logoutButton1.setBounds(1100, 10, 150, 30);
		logoutButton1.setForeground(Color.white);
		logoutButton1.setBackground(Color.darkGray);
		Panel.add(logoutButton1);
		
		logoutButton2 = new JButton("Logout");
		logoutButton2.setBounds(1100, 10, 150, 30);
		logoutButton2.setForeground(Color.white);
		logoutButton2.setBackground(Color.darkGray);
		ordersPanel.add(logoutButton2);
		
		logoutButton3 = new JButton("Logout");
		logoutButton3.setBounds(1100, 10, 150, 30);
		logoutButton3.setForeground(Color.white);
		logoutButton3.setBackground(Color.darkGray);
		usersPanel.add(logoutButton3);
		
	}
	
	public void insertMovie(String[] row) {
		moviesDTM.addRow(row);
	}
	
	public String getSearchedMovie() {
		return searchTF.getText();
	}
	
	public Movie getSelectedMovie() {
		Movie movie = new Movie();
		int row = moviesTable.getSelectedRow();
		if(row>=0) {
			movie.setId(Integer.parseInt(moviesDTM.getValueAt(row, 0).toString()));
			movie.setName(moviesDTM.getValueAt(row, 1).toString());
			movie.setRegizor(moviesDTM.getValueAt(row, 2).toString());
			movie.setDescription(moviesDTM.getValueAt(row, 3).toString());
			movie.setRating(Double.valueOf(moviesDTM.getValueAt(row, 4).toString()));
			movie.setGenre(moviesDTM.getValueAt(row, 5).toString());
			movie.setQuantity(Integer.parseInt(moviesDTM.getValueAt(row, 6).toString()));
			movie.setPrice(Double.valueOf(moviesDTM.getValueAt(row, 7).toString()));
		}
		moviesTable.getSelectionModel().clearSelection();
		
		return movie;
	}
	
	public void clearMoviesTable() {
		for(int i=0; i < moviesDTM.getRowCount(); i++)
			moviesDTM.removeRow(i);
		moviesDTM.setRowCount(0);
	}
	
	public Movie getMovieToCreate() {
		Movie movie = new Movie(0, createMovieNameTF.getText(), createRegizorTF.getText(), createDescriptionTF.getText(), Double.parseDouble(createRatingTF.getText()), createGenreTF.getText(), Integer.parseInt(createQuantityTF.getText()), Double.parseDouble(createPriceTF.getText()));
	
		return movie;
	}
	
	public Movie getMovieToUpdate() {
		Movie movie = selectedMovie;
		movie.setName(updateMovieNameTF.getText());
		movie.setRegizor(updateRegizorTF.getText());
		movie.setDescription(updateDescriptionTF.getText());
		movie.setRating(Double.parseDouble(updateRatingTF.getText()));
		movie.setGenre(updateGenreTF.getText());
		movie.setQuantity(Integer.parseInt(updateQuantityTF.getText()));
		movie.setPrice(Double.parseDouble(updatePriceTF.getText()));
		
		return movie;
	}
	
	public Movie getMovieToDelete() {
		return selectedMovie;
	}
	
	public User getCurrentUser() {
		return currentUser;
	}
	
	public int getSearchedUser() {
		return Integer.parseInt(searchUserTF.getText());
	}
	
	public void insertUser(String[] row) {
		usersDTM.addRow(row);
	}
	
	public void clearUsersTable() {
		for(int i=0; i < usersDTM.getRowCount(); i++)
			usersDTM.removeRow(i);
		usersDTM.setRowCount(0);
	}
	
	public User getSelectedUser() {
		User user = new User();
		int row = usersTable.getSelectedRow();
		if(row>=0) {
			user.setId(Integer.parseInt(usersDTM.getValueAt(row, 0).toString()));
			user.setName(usersDTM.getValueAt(row, 1).toString());
			user.setCnp(usersDTM.getValueAt(row, 2).toString());
			user.setAddress(usersDTM.getValueAt(row, 3).toString());
			user.setUsername(usersDTM.getValueAt(row, 4).toString());
			user.setPassword(usersDTM.getValueAt(row, 5).toString());
			user.setType(usersDTM.getValueAt(row, 6).toString());
		}
		usersTable.getSelectionModel().clearSelection();
		
		return user;
	}
	
	public int getSerchedUser() {
		return Integer.parseInt(searchUserTF.getText());
	}
	
	public User getUserToCreate() {
		User user = new User(0, createNameTF.getText(), createCNPTF.getText(), createAddressTF.getText(), createUsernameTF.getText(), createPasswordTF.getText(), createTypeTF.getText());
		
		return user;
	}
	
	public User getUserToUpdate() {
		User user = selectedUser;
		user.setName(updateNameTF.getText());
		user.setCnp(updateCNPTF.getText());
		user.setAddress(updateAddressTF.getText());
		user.setUsername(updateUsernameTF.getText());
		user.setPassword(updatePasswordTF.getText());
		user.setType(updateTypeTF.getText());
		
		return user;
	}
	
	public User getUserToDelete() {
		return selectedUser;
	}
	
	public void clearOrdersTable() {
		for(int i=0; i < ordersDTM.getRowCount(); i++)
			ordersDTM.removeRow(i);
		ordersDTM.setRowCount(0);
	}
	
	public void insertOrder(String[] row) {
		ordersDTM.addRow(row);
	}
	
	public Order getSelectedOrder() {
		Order order = new Order();
		DateFormat df = new SimpleDateFormat("yyy-MM-dd"); 
		int row = ordersTable.getSelectedRow();
		if(row>=0) {
			order.setId(Integer.parseInt(ordersDTM.getValueAt(row, 0).toString()));
			order.setUserId(Integer.parseInt(ordersDTM.getValueAt(row, 1).toString()));
			order.setMovieId(Integer.parseInt(ordersDTM.getValueAt(row, 2).toString()));
			order.setQuantity(Integer.parseInt(ordersDTM.getValueAt(row, 3).toString()));
			order.setPrice(Double.parseDouble(ordersDTM.getValueAt(row, 4).toString()));
			try {
				order.setDate(new Date(df.parse(ordersDTM.getValueAt(row, 5).toString()).getTime()));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			order.setStatus(ordersDTM.getValueAt(row, 6).toString());
		}
		ordersTable.getSelectionModel().clearSelection();
		
		return order;
	}
	
	public int getSearchedOrder() {
		return Integer.parseInt(searchOrderTF.getText());
	}
	
	public String getNewOrderStatus() {
		return changeStatusOrderTF.getText();
	}
	
	public Order getOrderToUpdate() {
		return selectedOrder;
	}
	
}
